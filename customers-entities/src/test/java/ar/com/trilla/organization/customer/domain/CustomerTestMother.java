package ar.com.trilla.organization.customer.domain;

import java.util.UUID;

public class CustomerTestMother {
    protected static final UUID CUSTOMER_ID = UUID.randomUUID();
    protected static final String CUSTOMER_NAME = "customerName";
    protected static final String CUSTOMER_DESCRIPTION = "customerDescription";
}
