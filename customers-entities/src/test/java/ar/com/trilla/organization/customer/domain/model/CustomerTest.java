package ar.com.trilla.organization.customer.domain.model;

import ar.com.trilla.organization.customer.domain.CustomerTestMother;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest extends CustomerTestMother {
    @Test
    void given_non_null_parameters_when_Customer_instantiation_is_executed_then_return_new_Customer_instance() {
        Customer customer = new Customer(CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_DESCRIPTION);

        assertNotNull(customer);
        assertEquals(CUSTOMER_ID, customer.id());
        assertEquals(CUSTOMER_NAME, customer.name());
        assertEquals(CUSTOMER_DESCRIPTION, customer.description());
    }

    @Test
    void given_null_description_when_Customer_instantiation_is_executed_then_return_new_Customer_instance() {
        Customer customer = new Customer(CUSTOMER_ID, CUSTOMER_NAME, null);

        assertNotNull(customer);
        assertEquals(CUSTOMER_ID, customer.id());
        assertEquals(CUSTOMER_NAME, customer.name());
        assertNull(customer.description());
    }

    @Test
    void given_null_id_when_Customer_instantiation_is_executed_then_throws_NullPointerException() {
        assertThrows(
                NullPointerException.class,
                () -> new Customer(null, CUSTOMER_NAME, CUSTOMER_DESCRIPTION)
        );
    }

    @Test
    void given_null_name_when_Customer_instantiation_is_executed_then_throws_NullPointerException() {
        assertThrows(
                NullPointerException.class,
                () -> new Customer(CUSTOMER_ID, null, CUSTOMER_DESCRIPTION)
        );
    }
}