package ar.com.trilla.organization.customer.domain.model;

import java.util.Objects;
import java.util.UUID;

public class Customer {
    private UUID id;
    private String name;
    private String description;

    public Customer(UUID id, String name, String description) {
        Objects.requireNonNull(id, "Customer.id must not be null");
        Objects.requireNonNull(name, "Customer.name must not be null");
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public UUID id() {
        return id;
    }

    public String name() {
        return name;
    }

    public String description() {
        return description;
    }

    public Customer update(String name, String description) {
        Objects.requireNonNull(name, "Customer.name must not be null");
        return new Customer(id, name, description);
    }
}
