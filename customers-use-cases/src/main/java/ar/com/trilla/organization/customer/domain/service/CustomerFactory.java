package ar.com.trilla.organization.customer.domain.service;

import ar.com.trilla.organization.customer.domain.model.Customer;
import ar.com.trilla.organization.customer.use_cases.request.CreateCustomerUseCaseRequest;

public interface CustomerFactory {
    Customer create(CreateCustomerUseCaseRequest request);
}
