package ar.com.trilla.organization.customer.use_cases.validation;

import ar.com.trilla.organization.customer.domain.exception.InvalidRequestException;
import ar.com.trilla.organization.customer.use_cases.request.CreateCustomerUseCaseRequest;

public class RequestValidations {
    public static void checkNotNull(CreateCustomerUseCaseRequest request, String errorMessage) {
        if (request == null) {
            throw new InvalidRequestException(errorMessage);
        }
    }
}
