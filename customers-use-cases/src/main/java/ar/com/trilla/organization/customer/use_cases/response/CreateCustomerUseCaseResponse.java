package ar.com.trilla.organization.customer.use_cases.response;

import ar.com.trilla.organization.customer.domain.model.Customer;

public record CreateCustomerUseCaseResponse(Customer customer) {
}
