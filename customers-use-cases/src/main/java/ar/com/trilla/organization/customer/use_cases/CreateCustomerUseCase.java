package ar.com.trilla.organization.customer.use_cases;

import ar.com.trilla.organization.customer.domain.model.Customer;
import ar.com.trilla.organization.customer.domain.service.CustomerFactory;
import ar.com.trilla.organization.customer.domain.service.CustomerGateway;
import ar.com.trilla.organization.customer.use_cases.request.CreateCustomerUseCaseRequest;
import ar.com.trilla.organization.customer.use_cases.response.CreateCustomerUseCaseResponse;

import java.util.concurrent.CompletableFuture;

public class CreateCustomerUseCase {
    private final CustomerFactory customerFactory;
    private final CustomerGateway customerGateway;

    public CreateCustomerUseCase(CustomerFactory customerFactory, CustomerGateway customerGateway) {
        this.customerFactory = customerFactory;
        this.customerGateway = customerGateway;
    }

    public CompletableFuture<CreateCustomerUseCaseResponse> handle(CreateCustomerUseCaseRequest request) {
        Customer customer = customerFactory.create(request);
        return customerGateway.save(customer)
                .thenApply(CreateCustomerUseCaseResponse::new);
    }
}
