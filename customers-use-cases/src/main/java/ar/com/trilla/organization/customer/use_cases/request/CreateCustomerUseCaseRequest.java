package ar.com.trilla.organization.customer.use_cases.request;

public record CreateCustomerUseCaseRequest(String name, String description) {
}
