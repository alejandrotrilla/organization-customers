package ar.com.trilla.organization.customer.domain.service;

import ar.com.trilla.organization.customer.domain.model.Customer;

import java.util.concurrent.CompletableFuture;

public interface CustomerGateway {
    CompletableFuture<Customer> save(Customer customer);
}
