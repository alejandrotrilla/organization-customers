package ar.com.trilla.organization.customer;

import ar.com.trilla.organization.customer.domain.exception.InvalidRequestException;
import ar.com.trilla.organization.customer.domain.model.Customer;
import ar.com.trilla.organization.customer.domain.service.CustomerFactory;
import ar.com.trilla.organization.customer.domain.service.CustomerGateway;
import ar.com.trilla.organization.customer.use_cases.request.CreateCustomerUseCaseRequest;
import org.mockito.Mock;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CustomerTestMother {
    protected static final UUID CUSTOMER_ID = UUID.randomUUID();
    protected static final String CUSTOMER_NAME = "customerName";
    protected static final String CUSTOMER_DESCRIPTION = "customerDescription";
    protected static final Customer CUSTOMER = new Customer(CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_DESCRIPTION);
    protected static final CreateCustomerUseCaseRequest VALID_CUSTOMER_CREATION_REQUEST = new CreateCustomerUseCaseRequest(CUSTOMER_NAME, CUSTOMER_DESCRIPTION);

    @Mock
    protected CustomerFactory customerFactory;

    @Mock
    protected CustomerGateway customerGateway;

    protected void mockFactoryCreateReturnInstance() {
        when(customerFactory.create(VALID_CUSTOMER_CREATION_REQUEST))
                .thenReturn(CUSTOMER);
    }

    protected void mockGatewaySaveInstance() {
        when(customerGateway.save(any(Customer.class)))
                .thenReturn(CompletableFuture.completedFuture(CUSTOMER));
    }

    protected void verifyGatewaySaveCalledOnce() {
        verify(customerGateway, times(1)).save(CUSTOMER);
    }

    protected void mockFactoryCreateThrowsException() {
        when(customerFactory.create(null))
                .thenThrow(new InvalidRequestException("Invalid request"));
    }
}
