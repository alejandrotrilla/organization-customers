package ar.com.trilla.organization.customer.use_cases;

import ar.com.trilla.organization.customer.CustomerTestMother;
import ar.com.trilla.organization.customer.domain.exception.InvalidRequestException;
import ar.com.trilla.organization.customer.use_cases.response.CreateCustomerUseCaseResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class CreateCustomerUseCaseTest extends CustomerTestMother {

    @InjectMocks
    private CreateCustomerUseCase target;

    @Test
    void given_valid_request_when_Customer_creation_is_executed_then_return_new_Customer_instance() {
        mockCreateAndSaveInstanceScenario();
        target.handle(VALID_CUSTOMER_CREATION_REQUEST).thenAccept(this::validateResponseAndVerifySave);
    }

    @Test
    void given_null_request_when_Customer_creation_is_executed_then_return_throws_InvalidRequestException() {
        mockFactoryCreateThrowsException();
        assertThrows(
                InvalidRequestException.class,
                () -> target.handle(null)
        );
    }

    private void mockCreateAndSaveInstanceScenario() {
        mockFactoryCreateReturnInstance();
        mockGatewaySaveInstance();
    }

    private void validateResponseAndVerifySave(CreateCustomerUseCaseResponse response) {
        assertNotNull(response);
        verifyGatewaySaveCalledOnce();
    }
}